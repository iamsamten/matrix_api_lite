import 'authentication_data.dart';
import 'authentication_types.dart';

class AuthenticationRegistrationToken extends AuthenticationData {
  String token;
  AuthenticationRegistrationToken({String? session, required this.token})
      : super(
          type: AuthenticationTypes.registrationToken,
          session: session,
        );

  AuthenticationRegistrationToken.fromJson(Map<String, dynamic> json)
      : token = json['token'] as String,
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    final data = super.toJson();
    data['token'] = token;
    return data;
  }
}
